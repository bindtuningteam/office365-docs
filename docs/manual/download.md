To **manually install** your theme, you'll have to **download** the corresponding package.

1. Access your account at <a href="https://bindtuning.com" target="_blank">BindTuning</a>;
2. Go to the **Design** feature and  switch to the **My Themes** tab.
3. Mouse hover the theme and click **More Details** to open the Theme details page;

    ![theme-select.png](..\images\select-theme.png)

4. Last but not least, click on **Download**.

    ![download-theme](..\images\download-theme.png)