If you want to backup your settings and content, or easily propagate them into other site collections, follow these steps:

---

#### Export Settings

To export your settings and content,   open the settings panel and click **Export**. This will generate a JSON file that you can select when prompted.
	
<img src="../../../images/export-btsettings.png" alt="BindTuning Settings Backup"	title="BindTuning Settings Backup" width="300" />

----

#### Import Settings

1. To import your settings and content open the settings panel and click **import**. You will be prompted to select a file. Select the JSON file you’ve exported.

1. Click **Confirm** when the confirmation modal shows up and you’re done!
	
  <img src="../../../images/import-btsettings.png" alt="BindTuning Settings Import and Export"	title="BindTuning Settings Import and Export" width="300" />