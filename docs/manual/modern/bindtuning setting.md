With the BindTuning SPFx Themes you can decide how the page is affected by the theme. To do this follow these steps:


1. On the command bar click on **BindTuning Settings** option;

![bindtuning-settings-button.png](../../images/bindtuning-settings-button.png)

<p class="alert alert-success">Alternatively you can open up your browser’s inspector by pressing F12 and on the console and typing <strong>“btOpenSettings()”</strong>.</p>

2. A settings panel will open from the right. The panel is divided into 7 sections:

    - <a href="../settings/general">General</a>
    - <a href="../settings/navigation">Navigation</a>
    - <a href="../settings/search">Search</a>
    - <a href="../settings/actions">Actions</a>
    - <a href="../settings/developer">Developer</a>
    - <a href="../settings/advanced">Advanced</a>
    - <a href="../settings/userinfo">User Info</a>