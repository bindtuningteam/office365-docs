**Note:** If you've already downloaded our **Provisioning Engine desktop app**, follow the instructions below; if not, follow the instructions <a href="../download">here</a>.

---
### Installation

<p class="alert alert-success"> The <strong>Provisioning Engine desktop application</strong> will only display the themes that have been generated on your BindTuning account.
</p>

1. Open the desktop **Provisioning Engine**. 

    ![provisioning-homescreen](../../images/provisioning-homescreen.png)

1. Login using your BindTuning account.

    <p class="alert alert-warning">Make sure to toggle the <strong>Office365</strong> option</p> 

    -  Input either your **Tenant** (allowing you to install on multiple site collections) or the particular **Site Collection** you want to install to;
    - Enter your Office365 email or user;
    - Enter your password. 

1. Navigate to the **Install** tab. 

1. Select the theme you intend to install and click on **Review&Install**.

    ![review-and-install](../../images/review-and-install.png)

1. Review the products to be installed and click **Install**. 

1. Choose the target for the installation:

    - **Classic** SharePoint Online experience;
    - **Modern** SharePoint Online experience and, subsequently, if you want to deploy the product on your **Tenant** or specific **Site Collection**.

    ![review-products](../../images/review-products.png)

1. Review the installation, click on **Accept and procced** and click **Install**. 

1. The installation process will begin, immediately, and you'll be able to check its status, as well as to report any errors during the installation. 

    ![installation-report](../../images/installation-report.png)

You're done! ✅

<!-- ---
### Apply the theme

To apply the theme, follow the instructions [here](../../../docs/automated/apply.md).
     -->


