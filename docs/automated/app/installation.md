The BindTuning app allows for the easy deployment of themes on Office 365

<p class="alert alert-success">This installation method is only valid for Office 365 (SharePoint Online).</p>

1. Login to your BindTuning account. 
    <p>
1. Navigate to the **Design** tab and click on **Themes Gallery**
    <p>

    ![design-tab](../../images/design-tab.png)
     
1. Mouse over the selected theme and click on **More Details**
    <p>

    ![select-theme](../../images/select-theme.png)
     
1. Proceed with either:
    * **Install**: Installs the default theme customization (including colors, fonts, elements, etc)
    * **Brand This Theme**: Opens the BindTuning customizer, where you'll be able to modify the theme (including colors, fonts, elements, etc)
    
    <p>

    ![install-bindtuning-app](../../images/install-bindtuning-app.png)
    
1. After both the customization and installation, provide both      your Site Collection URL and corresponding Office 365 user      email  
    <p>

1. Select the **SharePoint experience** you want to deploy to, as well as correlative **installation scope**:
    
    - **Classic:** Deploys the theme on **Classic SharePoint**, being available at the site collection level;
    - **Modern:** Deploys the theme on **Modern SharePoint**, being available at either **site collection** or **tenant** level.

    ![credentials-app.png](../../images/credentials-app.png)

    **Note:** Despite adding the solution to your **Tenant App Catalog**, only the **site collection** added in **step 5** will reflect the theme. From there, you can choose to either apply the theme to a selected number of site collections or to every site collection within your tenant:<p>
    - To apply the theme to **selected** site collections, follow the steps <a href="https://bindtuning-office-365-themes.readthedocs.io/en/latest/modern/installation/">here</a>; <p>
    - To apply the theme to **all** site collections across your tenancy, follow the steps <a href="https://support.bindtuning.com/hc/en-us/articles/360022325092-Activate-Modern-theme-for-the-tenant">here</a>.

1. Verify all the provided information and click **Confirm** 
    <p>

    ![confirm-installation](../../images/confirm-installation.png)
   
1. The installation will proceed in the background. To review it's progress click on **Recent Activity** (graph icon).
    <p>

    ![check-installation-progress](../../images/check-installation-progress.png)
<p>

After the installation has been completed, an alert will appear, informing you of its status.

![status](../../images/status.png)

<!-- ---
### Apply the theme

To apply the theme, follow the instructions [here](../../../docs/automated/apply.md). -->