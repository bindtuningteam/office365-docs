## Sandbox

To apply the theme using the Sandbox Solution you'll have to set the ***masterpage***. Instructions on how to complete the process can be found <a href="https://bindtuning-office-365-themes.readthedocs.io/en/latest/manual/sandbox/installation/">here</a>.
___
## Modern Experience 

After the installation has been successful, the theme should be, automatically, applied on the specified Site Collection. If that seems not to be the case, follow the below mentioned steps. 

1. Open the Site Collection where the theme has been deployed
1. Click on the cog (⚙️) icon.
1. Click on **Change the look**
1. Select the **Theme** option
1. Select the installed theme and hit **Save**
    <p>

    ![change-theme.png](../images/change-theme.png)

