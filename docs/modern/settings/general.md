Settings affecting the look and feel of the page.

<img src="../../../images/bindtuning-settings-general.png" alt="BindTuning Settings General"	title="BindTuning Settings General" width="300" />

---

#### Favicon

Sets a custom Favicon for your site collection. 

<p class="alert alert-info"><b>Not sure what a Favicon is?</b> <br>
Favicon is an icon that is picked up from the website to accompany the site name in the browser tabs and the Bookmarks/Favorites bar.</p>

![16-favicon.png](../../images/16.favicon.png)

---

#### Logo image URL

Sets a custom relative URL for your logo to pull from. Leave empty if you don't want your logo to link anywhere.

---

#### Logo Link URL

Sets a custom URL for your logo to link to. Leave empty if you don't want your logo to link anywhere.

Can also be used tokens to get the relative URL of the:<br>
- current site: **{site}** - The use of this token can be useful when,the theme is applied to a site collection with sub-sites, the logo will be linked to each current site.<br>
- site collection: **{sitecollection}** - The use of this token can be useful when, the theme is applied to a site collection and his sub-sites and each one has the logo will be linked to parent site collection.<br><br>

The use of both tokens can be useful when, the theme is applied to an Hub and associated sites and each one has to have the logo linked to the current site collection.

---

#### Page title link URL 

Sets a custom link on the page title. Leave empty if you don't want to display said link.

Can also be used tokens to get the relative URL of the:<br>
- current site: **{site}** - The use of this token can be useful when,the theme is applied to a site collection with sub-sites, the page title will be linked to each current site.<br>
- site collection: **{sitecollection}** - The use of this token can be useful when, the theme is applied to a site collection and his sub-sites and each one has the page title will be linked to parent site collection.<br><br>

The use of both tokens can be useful when, the theme is applied to an Hub and associated sites and each one has to have the page title linked to the current site collection.
    
---

#### Compact Styling

This will reduce the spacing between elements like the logo and navbar, paddings in the footer and header and possibly other elements. It works to increase the available content area.

---

#### Hide native hub navigation

Hides native hub navigation

---

#### Hub navigation position

Have the hub navigation after or before the theme header

---

#### Hide native search

Hide native SharePoint search

---

#### Hide sidebar

Hides team site sidebar

---

#### Hide Page Title
Hides theme’s page title.

---

#### Hide Footer

Allows you to hide the footer, increasing the available content area. 

---

#### Hide Breadcrumb

Hide Breadcrumb from the SharePoint site.

---

#### Headings

Apply theme heading typefaces to SPFx content and web parts. 