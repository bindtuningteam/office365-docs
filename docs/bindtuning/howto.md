BindTuning provides you with the flexibility to install our products the way you want to, regardless of technical knowledge or experience. 
<br>

Following this, Bindtuning offers two main installation processes:

- **Automated** installation;
- **Manual** installation. 

**Note:** We recommend the **Automated** installation, due to its easy deployment process. 

---
### Automated Installation

The **Automated** installation process makes the deployment of BindTuning's products as easy as a simple click. Depending on your needs, you'll be able to choose how to proceed with the installation.
<br>

Bindtuning offers two ways of, automatically, deploying your products: 

- **BindTuning app**;
- **Provisioning Engine desktop app**.

#### BindTuning app

The **BindTuning app** offers the possibility to deploy, your products to SharePoint Online, using our dedicated website. 
<br>


<a href="https://bindtuning-office-365-themes.readthedocs.io/en/latest/automated/app/installation/">Here</a> you'll learn how to install the products using the **BindTuning app**.

#### Provisioning Engine desktop app

The **Provisioning Engine** will also automatically deploy your Products, to both SharePoint Online and SharePoint on-premises. 
<br>

<a href="https://bindtuning-office-365-themes.readthedocs.io/en/latest/automated/provisioning/installation/">Here</a> you'll learn how to install the products using the **Provisioning desktop app**.

The main difference between the **BindTuning app** and the **Provisioning Engine** resides in the fact that the **BindTuning app** only supports Office 365, while the **Provisioning Engine** supports both Office 365 and SharePoint On-Premises. 

---

### Manual installation

The **Manual** installation process allows for more granular control on the installation with an added complexity layer, not present in the **Automated** installation. 

The **Manual** installation for Office365 will depend on the particular experience present on your site:

- For the **Classic** experience, the installation process can be found <a href="https://bindtuning-office-365-themes.readthedocs.io/en/latest/manual/sandbox/requirements/">here</a>;

- For the **Modern** experience, the installation process can be found <a href="https://bindtuning-office-365-themes.readthedocs.io/en/latest/modern/requirements/">here</a>.



