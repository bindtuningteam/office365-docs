To update the BindTuning theme on your SharePoint site you need first to update them on your BindTuning account.

1. Login on your **BindTuning account** at BindTuning;
2. Navigate to your **Themes Gallery**
3. If an update for the theme(s) is available, you'll be able to see a red circle.

    ![theme-update-notification](../images/theme-update-notification.png)

4. Mouse over the desired Theme and click on **More Details**. 

    ![update-theme.png](..\images\update-theme.png)

    - Click the **Update Now!** button to get the latest Theme files.
    
    - After the update has finish, follow **step 5**.

5. Click to **Download the theme**.

    